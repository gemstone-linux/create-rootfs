#!/bin/bash -e
#
# x86_64-build-packages-with-tools is a script to build the base quary packages using a pre-built toolchain for x86_64.
#

ROOTFS=${1%/}

if [ ! -d "$1" ]; then
  echo "Directory $1 DOES NOT exist."
  exit 1
fi

if [ ! -d $ROOTFS/tools ]; then
  echo "Tools directory could not be found."
  exit 1
fi

### ENV ###
LC_ALL=POSIX
PATH=/bin:/usr/bin:/sbin
MAKEFLAGS="-j4"
export LC_ALL PATH MAKEFLAGS
### END ###

PKG=(filesystem
  linux-api-headers
  man-pages
  glibc
  tzdata
  zlib
  file
  readline
  m4
  bc
  binutils
  gmp
  mpfr
  mpc
  shadow
  gcc
  bzip2
  pkg-config
  ncurses
  attr
  acl
  libcap
  sed
  psmisc
  iana-etc
  bison
  flex
  grep
  bash
  libtool
  gdbm
  gperf
  expat
  inetutils
  perl
  xml-parser
  intltool
  autoconf
  automake
  xz
  kmod
  gettext
  elfutils
  libffi
  openssl
  python
  ninja
  meson
  systemd
  procps-ng
  e2fsprogs
  coreutils
  check
  diffutils
  gawk
  findutils
  groff
  grub
  less
  gzip
  iproute2
  kbd
  libpipeline
  make
  patch
  dbus
  util-linux
  man-db
  tar
  texinfo
  vim
  mkinitramfs
  cpio
  linux
)

#echo "=====> Here is your enviroment..."
#env
#sleep 10

#if [ ! -h /tools ] ; then
#   echo "=====> No tools linked to toolchain..."
#   exit 99
#fi

create_chroot() {
  echo "======> Creating chroot dirs..."
  mkdir -pv $ROOTFS/{dev,proc,sys,run,etc,bin,sbin}
  mkdir -pv $ROOTFS/{boot,etc/{opt,sysconfig},home,lib/firmware,mnt,opt}
  mkdir -pv $ROOTFS/var/log
  mkdir -pv $ROOTFS/usr/{bin,lib}
  install -dv -m 0750 $ROOTFS/root
  install -dv -m 1777 $ROOTFS/tmp $ROOTFS/var/tmp
  mkdir -pv $ROOTFS/usr/{,local/}{bin,include,lib,sbin,src}
  mkdir -pv $ROOTFS/usr/{,local/}share/{color,dict,doc,info,locale,man}
  mkdir -v $ROOTFS/usr/{,local/}share/{misc,terminfo,zoneinfo}
  mkdir -v $ROOTFS/usr/libexec
  mkdir -pv $ROOTFS/usr/{,local/}share/man/man{1..8}

  mknod -m 600 $ROOTFS/dev/console c 5 1 || true
  mknod -m 666 $ROOTFS/dev/null c 1 3 || true

  echo "======> Creating resolv file..."
  echo "nameserver 1.1.1.1" >$ROOTFS/etc/resolv.conf

  echo "======> Copying mkpkg configuration file..."
  mkdir -p $ROOTFS/etc/mkpkg/
  cp $ROOTFS/tools/etc/mkpkg/mkpkg.conf $ROOTFS/etc/mkpkg/mkpkg.conf
  echo "MKPKG_PORTS=/ports" >$ROOTFS/etc/mkpkg/mkpkg.conf

  echo "======> Linking chroot with tools..."
  ln -sv /tools/bin/{bash,cat,chmod,dd,echo,ln,mkdir,pwd,rm,stty,touch} $ROOTFS/bin
  ln -sv /tools/bin/{env,install,perl,printf} $ROOTFS/usr/bin
  ln -sv /tools/lib/libgcc_s.so{,.1} $ROOTFS/usr/lib
  ln -sv /tools/lib/libstdc++.{a,so{,.6}} $ROOTFS/usr/lib
  for lib in blkid lzma mount uuid; do
    ln -sv /tools/lib/lib$lib.so* $ROOTFS/usr/lib
  done
  ln -svf /tools/include/blkid $ROOTFS/usr/include
  ln -svf /tools/include/libmount $ROOTFS/usr/include
  ln -svf /tools/include/uuid $ROOTFS/usr/include
  install -vdm755 $ROOTFS/usr/lib/pkgconfig

  for pc in blkid mount uuid; do
    sed 's@tools@usr@g' $ROOTFS/tools/lib/pkgconfig/${pc}.pc >$ROOTFS/usr/lib/pkgconfig/${pc}.pc
  done

  ln -sv bash $ROOTFS/bin/sh

  echo "======> Create log files..."
  touch $ROOTFS/var/log/{btmp,lastlog,faillog,wtmp}

  echo "======> Create passwd file..."
  cat >$ROOTFS/etc/passwd <<"EOF"
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/dev/null:/bin/false
daemon:x:6:6:Daemon User:/dev/null:/bin/false
messagebus:x:18:18:D-Bus Message Daemon User:/var/run/dbus:/bin/false
nobody:x:99:99:Unprivileged User:/dev/null:/bin/false
EOF

  echo "======> Create group..."
  cat >$ROOTFS/etc/group <<"EOF"
root:x:0:
bin:x:1:daemon
sys:x:2:
kmem:x:3:
tape:x:4:
tty:x:5:
daemon:x:6:
floppy:x:7:
disk:x:8:
lp:x:9:
dialout:x:10:
audio:x:11:
video:x:12:
utmp:x:13:
usb:x:14:
cdrom:x:15:
adm:x:16:
messagebus:x:18:
input:x:24:
mail:x:34:
kvm:x:61:
wheel:x:97:
nogroup:x:99:
users:x:999:
EOF

}

adjust_toolchain() {
  mv -v /tools/bin/{ld,ld-old}
  mv -v /tools/$(uname -m)-pc-linux-gnu/bin/{ld,ld-old}
  mv -v /tools/bin/{ld-new,ld}
  ln -sv /tools/bin/ld /tools/$(uname -m)-pc-linux-gnu/bin/ld

  gcc -dumpspecs | sed -e 's@/tools@@g' \
    -e '/\*startfile_prefix_spec:/{n;s@.*@/usr/lib/ @}' \
    -e '/\*cpp:/{n;s@$@ -isystem /usr/include@}' >$(dirname $(gcc --print-libgcc-file-name))/specs

  echo 'int main(){}' >dummy.c
  cc dummy.c -v -Wl,--verbose &>dummy.log
  readelf -l a.out | grep ': /lib'

  grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log

  grep -B1 '^ /usr/include' dummy.log

  grep 'SEARCH.*/usr/lib' dummy.log | sed 's|; |\n|g'

  grep "/lib.*/libc.so.6 " dummy.log

  grep found dummy.log

  rm -v dummy.c a.out dummy.log
}

check_gcc() {
  echo "=====> Checking gcc..."
  echo 'int main(){}' >dummy.c
  cc dummy.c -v -Wl,--verbose &>dummy.log
  readelf -l a.out | grep ': /lib'

  echo "=====> Verify start files..."
  grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log

  echo "=====> Verify compiler is searching for the correct header files..."
  grep -B4 '^ /usr/include' dummy.log

  echo "=====> Verify linker search path..."
  grep 'SEARCH.*/usr/lib' dummy.log | sed 's|; |\n|g'

  echo "=====> Verify if we are using correct libc..."
  grep "/lib.*/libc.so.6 " dummy.log

  echo "=====> Verify dynamic linker..."
  grep found dummy.log

  echo "=====> Cleaning up..."
  rm -v dummy.c a.out dummy.log
}

mount_chroot() {
  echo "======> Mounting chroot..."
  mount -v --bind /dev $ROOTFS/dev
  mount -vt devpts devpts $ROOTFS/dev/pts -o gid=5,mode=620
  mount -vt proc proc $ROOTFS/proc
  mount -vt sysfs sysfs $ROOTFS/sys
  mount -vt tmpfs tmpfs $ROOTFS/run
  echo "======> Done mounting chroot..."
}

umount_chroot() {
  echo "======> Unmounting chroot..."
  umount -v $ROOTFS/dev/pts
  umount -v $ROOTFS/dev
  umount -v $ROOTFS/run
  umount -v $ROOTFS/proc
  umount -v $ROOTFS/sys
  echo "======> Done unmounting chroot..."
}

run_chroot() {
  trap umount_chroot HUP INT QUIT TERM SIGHUP SIGINT SIGQUIT SIGILL SIGABRT SIGKILL SIGTRAP SIGTERM SIGSTOP SIGSEGV
  
  sync

  echo "======> Running chroot in with command '$1'..."

  mount_chroot

  if [ -h $ROOTFS/dev/shm ]; then
    mkdir -p $ROOTFS/$(readlink $ROOTFS/dev/shm)
  fi
  
  local PATH 
  PATH=$ROOTFS/tools/bin:/bin:/usr/bin

  chroot "$ROOTFS" /tools/bin/env -i \
    HOME=/root \
    TERM="$TERM" \
    LC_ALL=POSIX \
    MAKEFLAGS="-j4" \
    MKPKG_LIB=/tools/lib/project-gemstone/libmkpkg.sh \
    PS1='\u:\w\$ ' \
    PATH=/bin:/usr/bin:/sbin:/usr/sbin:/tools/bin:/tools/sbin /tools/bin/bash --login +h -c "$@"

  CHROOT_EXIT=$?

  echo "Chroot exit statu: $CHROOT_EXIT"

  umount_chroot

  return $CHROOT_EXIT
}

make_rootfs() {
  mkdir
}

create_chroot

for p in ${PKG[@]}; do
  sync
  case $p in
  glibc)
    run_chroot "ln -sfv /tools/lib/gcc /usr/lib"
    run_chroot "cd /quary/base/$p && mkpkg --check"
    run_chroot "cd /usr && chmod o+w -R ."
    echo "======> Going to install $p from /quary/base/$p/$p-*.pkg..."
    run_chroot "tar --keep-directory-symlink -xavpf /quary/base/$p/$p-*.pkg -C /"
    echo "======> Adjusting toolchain..."
    run_chroot "$(type adjust_toolchain | sed '1,3d;$d')"
    run_chroot "rm -f /usr/lib/gcc"
    ;;
  bc)
    run_chroot "ln -sfv /tools/lib/libncursesw.so.6 /usr/lib/libncursesw.so.6"
    run_chroot "ln -sfv libncurses.so.6 /usr/lib/libncurses.so"
    run_chroot "cd /quary/base/$p && mkpkg"
    echo "======> Going to install $p from /quary/base/$p/$p-*.pkg..."
    run_chroot "rm /usr/lib/{libncurses.so,libncursesw.so.6}"
    run_chroot "tar --keep-directory-symlink -xavpf /quary/base/$p/$p-*.pkg -C /"
    ;;
  gcc)
    run_chroot "cd /quary/base/$p && mkpkg --check"
    run_chroot "rm /usr/lib/{libgcc_s.so{,.1},libstdc++.{a,so{,.6}}}"
    echo "======> Going to install $p from /quary/base/$p/$p-*.pkg..."
    run_chroot "tar --keep-directory-symlink -xavpf /quary/base/$p/$p-*.pkg -C /"
    run_chroot "$(type check_gcc | sed '1,3d;$d')"
    ;;
  bash)
    run_chroot "cd /quary/base/$p && mkpkg"
    echo "======> Going to install $p from /quary/base/$p/$p-*.pkg..."
    run_chroot "tar --keep-directory-symlink --overwrite -xavpf /quary/base/$p/$p-*.pkg -C /"
    ;;
  perl)
    run_chroot "rm /usr/bin/perl"
    run_chroot "cd /quary/base/$p && mkpkg"
    echo "======> Going to install $p from /quary/base/$p/$p-*.pkg..."
    run_chroot "tar --keep-directory-symlink -xavpf /quary/base/$p/$p-*.pkg -C /"
    ;;
  *)
    run_chroot "cd /quary/base/$p && mkpkg"
    echo "======> Going to install $p from /quary/base/$p/$p-*.pkg..."
    run_chroot "tar --keep-directory-symlink -xavpf /quary/base/$p/$p-*.pkg -C /"
    ;;
  esac
done

echo "======> Creating built package directory..."

run_chroot "mkdir -p /built"

for p in ${PKG[@]}; do
  echo "======> Moving $p package to built dir..."
  run_chroot "cp -v /quary/base/$p/$p-*.pkg /built"
done
